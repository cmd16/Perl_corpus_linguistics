This project contains a number of classes and functions that will be useful for corpus linguistics:

- read a corpus in from a plaintext file, directory of plaintext files, or plaintext file that contains filenames of plaintext files
- word tokenization (customizable with regular expressions; default is to split on anything that is not a letter or an apostrophe)
- create list of unique tokens and their frequencies
- search by token(s) to see a concordance-like view (customizable with regular expressions)
- n-grams (powered by NSP)
- keyword analysis using log-likelihood
- tag parts of speech using Lingua::EN::Tagger
- command line interface for using any of the previously mentioned tools

It is based off my initial project in Python, which can be found at https://gitlab.com/cmd16/Corpus-Linguistics